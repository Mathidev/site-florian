var navbarState = false;
var navStatementFixed = false;



/*window.onscroll = function() {
    if (navStatementFixed == false) {
        if (document.documentElement.scrollTop > 100|| document.body.scrollTop > 100) {
            if (navbarState == false)
                changeNavbarStatut(true);
        }
        else {
            if (navbarState)
                changeNavbarStatut(false);
        }
    }
    else {
        changeNavbarStatut(true);
    }
}

function navbarStatementFixed(state) {
    navStatementFixed = state;

    changeNavbarStatut(state);
}

function changeNavbarStatut(state) {
    var navbar = document.getElementById('navbar');
    var dropdowns = document.getElementsByClassName('dropdown');

    if (state) {
        navbar.style.backgroundColor = 'rgba(0, 0, 0, 0.8)';
        for (var i = 0; i < dropdowns.length; i++)
            dropdowns[i].style.backgroundColor = 'rgba(0, 0, 0, 0.8)';

    }
    else {
        navbar.style.backgroundColor = 'rgba(0, 0, 0, 0.2)';
        for (var i = 0; i < dropdowns.length; i++)
            dropdowns[i].style.backgroundColor = 'rgba(0, 0, 0, 0.5)';
    }
    navbarState = state;
}
*/


window.onresize = function() {
	var conteneur = document.getElementById("conteneur");
	var footer = document.getElementById("footer");
	var taille = conteneur.offsetHeight;
	var ecran = window.innerHeight;

	if (taille < ecran) {
		footer.style.position = "fixed";
		footer.style.bottom = 0;
        footer.style.width = "100%";
	}
	else{
		footer.style.position = "relative";
	}
}

function placementFooter() {
    var conteneur = document.getElementById("conteneur");
    var footer = document.getElementById("footer");
    var taille = conteneur.offsetHeight;
    var ecran = window.innerHeight;

    if (taille < ecran) {
        footer.style.position = "fixed";
        footer.style.bottom = 0;
        footer.style.width = "100%";
    }
    else{
        footer.style.position = "relative";
    }
}
